package com.whilerain.dartraystest;

import android.app.Application;
import android.content.Context;

/**
 * Created by shawn on 2017/3/28.
 */

public class App extends Application{
    private static Context instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = getApplicationContext();
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static Context getInstance() {
        return instance;
    }

}

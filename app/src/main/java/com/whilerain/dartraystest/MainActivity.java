package com.whilerain.dartraystest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.polidea.rxandroidble2.RxBleClient;
import com.whilerain.dartrayslib.DartRaysManager;
import com.whilerain.dartrayslib.command.BaseCommand;
import com.whilerain.dartrayslib.command.BaseRequest;
import com.whilerain.dartrayslib.command.IncomingCallCommand;
import com.whilerain.dartrayslib.command.IncomingCallOffHookCommand;
import com.whilerain.dartrayslib.command.IncomingCallWithNumbersCommand;
import com.whilerain.dartrayslib.command.IncomingFbCommand;
import com.whilerain.dartrayslib.command.IncomingLineCommand;
import com.whilerain.dartrayslib.command.IncomingSmsCommand;
import com.whilerain.dartrayslib.command.IncomingWechatCommand;
import com.whilerain.dartrayslib.command.InstantSpeedCommand;
import com.whilerain.dartrayslib.command.NaviComposedInfoCommand;
import com.whilerain.dartrayslib.command.NaviLeftTimeCommand;
import com.whilerain.dartrayslib.command.NaviStartedCommand;
import com.whilerain.dartrayslib.command.NaviStoppedCommand;
import com.whilerain.dartrayslib.command.NaviTurnDistanceCommand;
import com.whilerain.dartrayslib.command.NaviTurnModifierCommand;
import com.whilerain.dartrayslib.command.ShowNaviScreenCommand;
import com.whilerain.dartrayslib.command.ShowNormalScreenCommand;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private String allText = "";

    @BindView(R.id.state)
    TextView vState;

    @BindView(R.id.device)
    Button vDevice;

    @BindView(R.id.connect)
    Button vConnect;

    @BindView(R.id.checkBox_distance)
    CheckBox vCheckboxDistance;

    @BindView(R.id.checkBox_speed)
    CheckBox vCheckboxSpeed;

    @BindView(R.id.checkBox_time)
    CheckBox vCheckboxTime;

    @BindView(R.id.checkBox_combined)
    CheckBox vCheckboxCombined;

    @BindView(R.id.millisecs)
    EditText vMillisecs;

    @BindView(R.id.incomingcallnum)
    EditText vIncomingCallNum;

    int kmh = 0;
    int h = 0;
    int m = 0;
    int meter = 0;

    @OnClick(R.id.call)
    void onCall(){
        IncomingCallCommand cmd = new IncomingCallCommand();
        DartRaysManager.getInstance().enqueue(cmd, false);
        appendMessage("send --- " + cmd.debugInfo());
    }

    @OnClick(R.id.calloff)
    void onCallOff(){
        IncomingCallOffHookCommand cmd = new IncomingCallOffHookCommand();
        DartRaysManager.getInstance().enqueue(cmd, false);
        appendMessage("send --- " + cmd.debugInfo());
    }
    @OnClick(R.id.fb)
    void onFb() {
        IncomingFbCommand cmd = new IncomingFbCommand();
        DartRaysManager.getInstance().enqueue(cmd, false);
        appendMessage("send --- " + cmd.debugInfo());
    }

    @OnClick(R.id.line)
    void onLine() {
        IncomingLineCommand cmd = new IncomingLineCommand();
        DartRaysManager.getInstance().enqueue(cmd, false);
        appendMessage("send --- " + cmd.debugInfo());
    }

    @OnClick(R.id.wechat)
    void onWechat() {
        IncomingWechatCommand cmd = new IncomingWechatCommand();
        DartRaysManager.getInstance().enqueue(new IncomingWechatCommand(), false);
        appendMessage("send --- " + cmd.debugInfo());
    }

    @Override
    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
    }

    @OnClick(R.id.incomingcall)
    void onIncomingCall() {

        String num = vIncomingCallNum.getText().toString();
        IncomingCallWithNumbersCommand cmd = new IncomingCallWithNumbersCommand(num);
        DartRaysManager.getInstance().enqueue(cmd, false);
        appendMessage("send --- " + cmd.debugInfo());
    }

    @OnClick(R.id.device)
    void onDevice() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    123);
        } else {
            PickBleDeviceActivity.launchForResult(this);
        }

    }

    @OnClick(R.id.scan)
    void onScan() {
        String address = SharePrefHandler.getSharedPrefences().getString("dartrays_address", "");
        if (address.length() > 0) {
            DartRaysManager.getInstance().register(listener);
            DartRaysManager.getInstance().setup(this, address);
            DartRaysManager.getInstance().scan();
        } else {
            Toast.makeText(this, "No device selected", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.connect)
    void onConnect() {
        if (vConnect.getText().toString().equalsIgnoreCase("connect")) {
            DartRaysManager.getInstance().connect();
        } else {
            DartRaysManager.getInstance().disconnect();
            DartRaysManager.getInstance().destory();
            stopExeCmds();
        }
    }

    @OnClick(R.id.shownormal)
    void onNormal() {
        command = new ShowNormalScreenCommand();
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.shownavi)
    void onNavi() {
        command = new ShowNaviScreenCommand();
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.turnright)
    void onRight() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightTurn);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.turnleft)
    void onLeft() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftTurn);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.straight)
    void onStraight() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.straight);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.uturnright)
    void onUturnRight() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.uturnRight);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.uturnleft)
    void onUturnLeft() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.uturnLeft);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.rightside)
    void onRightSide() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightSide);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.leftside)
    void onLeftSide() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftSide);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.merge)
    void onMerge() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.merge);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.slightright)
    void onSlightRight() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.turnSlightRight);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.slightleft)
    void onSlightLeft() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.turnSlightLeft);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.destatright)
    void onDestAtRight() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.destinationAtRight);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.destatleft)
    void onDestAtLeft() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.destinationAtLeft);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.sharpright)
    void onSharpRight() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.sharpRightTurn);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.sharpleft)
    void onSharpLeft() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.sharpLeftTurn);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.rightfork)
    void onRightFork() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightFork);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.leftfork)
    void onLeftFork() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftFork);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.leftforks)
    void onLeftForkS(){
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftForkStraight);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.rightforks)
    void onRightForks(){
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightForkStraight);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.rightexit)
    void onRightExit() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightExit);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.leftexit)
    void onLeftExit() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftExit);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }


    @OnClick(R.id.rround1)
    void onR1() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightRoundaboutExit1);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.rround2)
    void onR2() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightRoundaboutExit2);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.rround3)
    void onR3() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightRoundaboutExit3);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.rround4)
    void onR4() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.rightRoundaboutExit4);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.lround1)
    void onL1() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftRoundaboutExit1);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.lround2)
    void onL2() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftRoundaboutExit2);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.lround3)
    void onL3() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftRoundaboutExit3);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.lround4)
    void onL4() {
        command = new NaviTurnModifierCommand(NaviTurnModifierCommand.Modifier.leftRoundaboutExit4);
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.startnavi)
    void onStartNavi(){
        command = new NaviStartedCommand();
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.stopnavi)
    void onStopNavi(){
        command = new NaviStoppedCommand();
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    @OnClick(R.id.sms)
    void onSms() {
        command = new IncomingSmsCommand();
        DartRaysManager.getInstance().enqueue(command, false);
        appendMessage("send --- " + command.debugInfo());
    }

    boolean shouldStop = false;

    CheckBox[] checkBoxes;
    int exeIndex = 0;
    BaseCommand command;
    Runnable exeRunnable = () -> {
        appendMessage("Start sending command");
        while (!shouldStop) {
            try {
                Thread.currentThread().sleep(Integer.valueOf(vMillisecs.getText().toString()));
                if (exeIndex >= checkBoxes.length) {
                    exeIndex = 0;
                }
                while (!checkBoxes[exeIndex].isChecked()) {
                    exeIndex += 1;
                    if (exeIndex >= checkBoxes.length) {
                        exeIndex = 0;
                    }
                }
                command = getCommand(checkBoxes[exeIndex]);
                DartRaysManager.getInstance().enqueue(command, false);
                appendMessage("send --- " + command.debugInfo());
                exeIndex++;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        appendMessage("Stop sending command");
    };

    private BaseCommand getCommand(CheckBox checkBox) {
        if (checkBox.equals(vCheckboxCombined)) {
            kmh = kmh > 100 ? 0 : kmh;
            meter = meter > 1200 ? 0 : meter;
            h = h > 3 ? 0 : h;
            m = m > 58 ? 0 : m;
            return new NaviComposedInfoCommand(h++, m++, meter++, kmh++);
        } else if (checkBox.equals(vCheckboxSpeed)) {
            kmh = kmh > 100 ? 0 : kmh;
            return new InstantSpeedCommand(kmh++);
        } else if (checkBox.equals(vCheckboxDistance)) {
            meter = meter > 1200 ? 0 : meter;
            return new NaviTurnDistanceCommand(meter++);
        } else {
            h = h > 3 ? 0 : h;
            m = m > 58 ? 0 : m;
            return new NaviLeftTimeCommand(h++, m++);
        }

    }

    DartRaysManager.Listener listener = new DartRaysManager.Listener() {
        @Override
        public void onDeviceFound() {
            appendMessage("Device found");
        }

        @Override
        public void onDeviceNotFound() {
            stopExeCmds();
            appendMessage("Device not found");
        }

        @Override
        public void onDeviceConnected() {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    appendMessage("Device connected");
                    vConnect.setText("Disconnect");
                }
            });
            startExeCmds();

        }

        @Override
        public void onRequestReceived(BaseRequest request) {
            appendMessage("received --- " + request.debugInfo());

        }

        @Override
        public void onDeviceDisconnected() {
            stopExeCmds();
            runOnUiThread(() -> {
                appendMessage("Device disconnected");
                vConnect.setText("Connect");
            });

        }

        @Override
        public void onFailState(RxBleClient.State state) {
            stopExeCmds();
            appendMessage("fail --- " + state.name());
        }
    };

    private void startExeCmds() {
        shouldStop = false;
        new Thread(exeRunnable).start();
    }

    private void stopExeCmds() {
        shouldStop = true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initViews();

    }

    private void initViews() {
        String name = SharePrefHandler.getSharedPrefences().getString("dartrays_name", "");
        if (name.length() > 0) {
            vDevice.setText(name);
        }

        checkBoxes = new CheckBox[]{vCheckboxCombined, vCheckboxSpeed, vCheckboxDistance, vCheckboxTime};
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        DartRaysManager.getInstance().unregister(listener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        DartRaysManager.getInstance().disconnect();
        DartRaysManager.getInstance().destory();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PickBleDeviceActivity.REQUEST_PICK_BLE && resultCode == RESULT_OK) {
            SharePrefHandler.getEditor().putString("dartrays_name", data.getStringExtra("name")).apply();
            SharePrefHandler.getEditor().putString("dartrays_address", data.getStringExtra("address")).apply();
            vDevice.setText(data.getStringExtra("name"));

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void appendMessage(String msg) {
        runOnUiThread(() -> {
            allText = msg + "\n" + allText;
            vState.setText(allText);
        });

    }
}

package com.whilerain.dartraystest;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.scan.ScanSettings;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

public class PickBleDeviceActivity extends AppCompatActivity {
    private static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_PICK_BLE = 122;

    @BindView(R.id.scan)
    Button vScan;

    @BindView(R.id.list)
    RecyclerView vRecycler;

    @BindView(R.id.progressBar)
    ProgressBar vProgress;

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @OnClick(R.id.scan)
    void onScan() {
        if (checkBluetooth()) {
            adapter.clear();
            addPairedList();

            if (rxBleClient == null) {
                rxBleClient = RxBleClient.create(this);
            }
            scanDisposable = rxBleClient.scanBleDevices(new ScanSettings.Builder().build())
                    .subscribe(scanResult -> {
                        RxBleDevice d = scanResult.getBleDevice();
                        adapter.add(d);
                    });

            new Handler().postDelayed(() -> {
                vScan.setVisibility(View.VISIBLE);
                vProgress.setVisibility(View.GONE);
                scanDisposable.dispose();
            }, 7000);

            vScan.setVisibility(View.GONE);
            vProgress.setVisibility(View.VISIBLE);
        }

    }

    Disposable scanDisposable;

    RxBleClient rxBleClient;

    /**
     * for blue tooth device search & scan
     */
    private BluetoothAdapter mBtAdapter;

    /**
     * Paired ble device
     */
    private List<RxBleDevice> pairedDevices = new ArrayList();
    /**
     * Recycler view adapter for device list
     */
    private ListAdapter adapter;


    public static void launchForResult(Activity activity) {
        activity.startActivityForResult(new Intent(activity, PickBleDeviceActivity.class), REQUEST_PICK_BLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_ble_device);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        addPairedList();
        onScan();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (scanDisposable != null)
            scanDisposable.dispose();
    }

    private void addPairedList() {

        if (checkBluetooth()) {
            if (rxBleClient == null) {
                rxBleClient = RxBleClient.create(this);
            }

            if (pairedDevices.size() == 0) {
                pairedDevices.addAll(rxBleClient.getBondedDevices());
            }
            adapter.add(pairedDevices);
        }
    }

    private boolean checkBluetooth() {
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth not supported at this device", Toast.LENGTH_LONG).show();
            return false;
        } else if (!mBtAdapter.isEnabled()) {
            enableBtDialog();

            return false;
        }
        return true;

    }

    private void enableBtDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable bluetooth")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    }
                })
                .create().show();
        return;
    }

    private void initViews() {
        setSupportActionBar(vToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Pick a bluetooth");


        vRecycler.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        vRecycler.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        adapter = new ListAdapter();
        vRecycler.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class ListAdapter extends RecyclerView.Adapter<VH> {
        List<RxBleDevice> list = new ArrayList<>();

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(PickBleDeviceActivity.this).inflate(R.layout.device_name, parent, false);
            VH vh = new VH(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position) {
            holder.loadData(list.get(position));
        }


        @Override
        public int getItemCount() {
            return list.size();
        }

        public void add(RxBleDevice d) {
            if(!isAvailable(d)) {
                list.add(d);
                notifyItemInserted(list.size() - 1);
            }
        }

        private boolean isAvailable(RxBleDevice d) {
            for(RxBleDevice device : list){
                if(device.getMacAddress().equals(d.getMacAddress())){
                    return true;
                }
            }
            return false;
        }

        public void add(List<RxBleDevice> devices) {
            list.addAll(devices);
            notifyDataSetChanged();
        }

        public void clear() {
            list.clear();
            notifyDataSetChanged();
        }
    }

    class VH extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.address)
        TextView vAddress;

        RxBleDevice device;

        public void loadData(RxBleDevice device) {
            this.device = device;
            String name = device.getName();
            vName.setText(name == null || name.length() == 0 ? "Unknow" : name);
            vAddress.setText(device.getMacAddress());
        }

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> {
                Intent it = new Intent();
                String name = device.getBluetoothDevice().getName();
                it.putExtra("name", name == null || name.length() == 0 ? "unknow" : name);
                it.putExtra("address", device.getBluetoothDevice().getAddress());
                setResult(RESULT_OK, it);
                finish();
            });
        }
    }
}

package com.whilerain.dartraystest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by shawn on 2017/4/28.
 */

public class SharePrefHandler {


    private static String file_key = "navier.pref";

    @Deprecated
    public static SharedPreferences getSharedPrefences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences getSharedPrefences(){
        return PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    }

    @Deprecated
    public static SharedPreferences.Editor getEditor(Context context){
        return getSharedPrefences(context).edit();
    }

    public static SharedPreferences.Editor getEditor(){
        return getSharedPrefences().edit();
    }


}
